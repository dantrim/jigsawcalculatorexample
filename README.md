# JigsawCalculatorExample

Example of some code that load the JigsawCalculator library

This package is an example how a user wishing to use [JigsawCalculator](https://gitlab.cern.ch/dantrim/jigsawcalculator)
in their analysis code would do so. This assumes that you have already successfully built
[JigsawCalculator](https://gitlab.cern.ch/dantrim/jigsawcalculator) and it is located
in your analysis directory where your analysis packages are.

## Installation and Running

This

```bash
cd <analysis-dir-where-analysis-packages-exist>/
lsetup "asetup AnalysisBase,21.2.55,here"
source jigsawcalculator/bash/setup.sh
git clone https://:@gitlab.cern.ch:8443/dantrim/jigsawcalculatorexample.git
cd ..
mkdir build/
cd build/
cmake ../source
make -j4
source x86*/setup.sh
```

At this point the executable ```jigsaw_example``` is available to you. Running
it produces output from a dummy event loop as follows:

```bash
$ jigsaw_example

RestFrames v1.0.0 -- Developed by Christopher Rogan (crogan@cern.ch)
                     Copyright (c) 2014-2016, Christopher Rogan
                     http://RestFrames.com

jigsaw::JigsawCalculator::initialize(...)   Initializing JigsawType: TTMET2LW
===========================================
 Starting event loop
- - - - - - - - - - - - - - - - - - - - - - - -
 Event #0

  MDR       = 63.214
  DBP       = 2.27891
  gamInvRp1 = 0.228884
  RPT       = 0.640036
- - - - - - - - - - - - - - - - - - - - - - - -
 Event #1

  MDR       = 226.135
  DBP       = 1.85377
  gamInvRp1 = 0.466885
  RPT       = 0.563059
- - - - - - - - - - - - - - - - - - - - - - - -
 Event #2

  MDR       = 107.81
  DBP       = 2.0759
  gamInvRp1 = 0.499317
  RPT       = 0.277899
- - - - - - - - - - - - - - - - - - - - - - - -
 Event #3

  MDR       = 334.05
  DBP       = 1.40719
  gamInvRp1 = 0.733774
  RPT       = 0.711888
- - - - - - - - - - - - - - - - - - - - - - - -
 Event #4

  MDR       = 449.617
  DBP       = 1.60315
  gamInvRp1 = 0.273658
  RPT       = 0.643829
===========================================
Event loop finished
```

You can see the source code for this executable [here](util/jigsaw_example.cxx). The
main parts being the initialization of the JigsawCalculator and loading in the
```object_map```.

You can see in the [CMakeLists.txt file](CMakeLists.txt) how to link your analysis
code to the JigsawCalculator library, ```JigsawCalculator``` (e.g. [here](CMakeLists.txt#L21)).

**You must call ```source jigsawcalculator/bash/setup.sh``` before compiling/running 
analysis code that relies on the ```JigsawCalculator``` libary.**

