//std/stl
#include <iostream>
#include <string>
#include <random>
#include <vector>
#include <map>
using namespace std;

//ROOT
#include "TLorentzVector.h"

//Jigsaw
#include "jigsawcalculator/JigsawCalculator.h"


int main()
{
    jigsaw::JigsawCalculator calculator;
    calculator.initialize("TTMET2LW");

    // mimic event loop
    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution(0.0, 5.0);

    std::map<std::string, std::vector<TLorentzVector>> object_map;

    cout << "===========================================" << endl;
    cout << " Starting event loop" << endl;
    for(int ievent = 0; ievent < 5; ievent++)
    {
        double num = distribution(generator);

        TLorentzVector met;
        TLorentzVector lepton0;
        TLorentzVector lepton1;

        met.SetPxPyPzE(43. * num, 17. * num, 0., 120. * num);
        lepton0.SetPtEtaPhiM(48. * num, 0.8 * num, 2.4 * num, 48 * num);
        lepton1.SetPtEtaPhiM(24. * num, 1.0 * num, -1.3 * num, 24. * num);

        // build the object map for the calculator
        // the TTMET2LW calculator expects "leptons" and "met"
        object_map["leptons"] = { lepton0, lepton1 };
        object_map["met"] = { met };

        // load the inputs and calculate the TTMET2LW observables for this event
        calculator.load_event(object_map);
        auto variables = calculator.variables();

        cout << "- - - - - - - - - - - - - - - - - - - - - - - -" << endl;
        cout << " Event #" << ievent << endl;
        cout << endl;
        cout << "  MDR       = " << variables.at("MDR") << endl;
        cout << "  DBP       = " << variables.at("DPB_vSS") << endl;
        cout << "  gamInvRp1 = " << variables.at("gamInvRp1") << endl;
        cout << "  RPT       = " << variables.at("RPT") << endl;

    } // ievent
    cout << "===========================================" << endl;
    cout << "Event loop finished" << endl;


    return 0;
}
